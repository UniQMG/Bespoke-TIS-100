# An interconnect script for filtering out data note events
# HOW TO USE: see example_node_2.py

def on_note(pitch, vel):
   Node.gate(pitch, vel, me.me())
