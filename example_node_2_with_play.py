# A TIS-100 node with a custom instruction for playing midi
# HOW TO USE:
# See example_node_1.py, then additionally wire output to an example_node_4_gate.py script
# then wire output from that script to an instrument

def on_pulse():
   node = Node.get(1);

   helper = ArgumentHelper()
   def play(pitch, vel, dur_num, dur_denum):
      helper.provide({ 'pitch': pitch, 'vel': vel, 'dur_num': dur_num, 'dur_denum': dur_denum })
      if helper.ready():
         pitch = helper.get('pitch')
         vel = helper.get('vel')
         dur = helper.get('dur_num') / helper.get('dur_denum')
         helper.reset();
         me.output(f"PLAY {pitch} {vel} {dur}");
         node.send_nonvalue_note(pitch, vel, dur);
      else:
         me.output(f"PLAY: blocked");
   node.define("PLAY", ["V", "V", "V", "V"], play)

   script = ""
   script += "ADD 60\n"
   script += "SWP\n"
   script += "LOOP:\n"
   script += "  SWP\n"
   script += "  PLAY ACC 60 1 16\n"
   script += "  ADD 1\n"
   script += "  SAV\n"
   script += "  SUB 72\n"
   script += "  JLZ LOOP\n"
   script += "END:"
   script += "  JRO 0\n"
   node.parse_script(script, me.me());
   node.step();
   node.status();
   me.set("a", node.IP)
   me.set("b", node.ACC)
   me.set("c", node.BAK)

def on_note(pitch, vel):
   me.output(f"p{pitch} v{vel}");
   Node.get(1).recv_note(pitch, vel);
