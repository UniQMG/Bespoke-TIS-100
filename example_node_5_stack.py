# Stack node
# Allows reading and writing values to a stack, direction agnostic
# HOW TO USE:
# similar to example_node_1.py

def recieved_value(side, value):
   me.output(f"recieved value {side} {value}")
   Node.get(6).stack.append(value)

def recieved_request(side):
   me.output(f"recieved request {side}")
   stack = Node.get(6).stack
   if len(stack) == 0: return
   node.send_value(side, stack.pop())

def on_pulse():
   node = Node.get(6);
   if not hasattr(node, "stack"):
      node.stack = []

   node.parse_script("", me.me())
   node.on("recieved_value", "handler", recieved_value)
   node.on("recieved_request", "handler", recieved_request)
   if len(node.stack) < 15:
      node.request_value("UP");
      node.request_value("LEFT");
      node.request_value("RIGHT");
      node.request_value("DOWN");

   me.set("a", len(node.stack))

def on_note(pitch, vel):
   Node.get(6).recv_note(pitch, vel);
