# The main Node class that provides functionality for all other scripts
# HOW TO USE:
# Create one instance and set execute priority below all other scripts

from functools import partial
from collections import defaultdict
import math
import re

class Node:
   idincr = 0
   nodes = defaultdict(lambda: Node())
   loghistory = []
   lognum = 0

   def get(id):
      return Node.nodes[id]

   def __init__(self):
      Node.idincr += 1
      self.id = Node.idincr

      self.script = [];
      self.lastScript = None
      self.IP = 0;
      self.instrExecuted = 0;
      self.madeProgression = True;
      self.ACC = 0;
      self.BAK = 0;
      self.LAST = None;
      self.ANY_ITER = 0;

      self.custom_registers = {}
      self.instructions = {}
      self.labels = {}

      self.node = None;
      self.clear_bufs()
      self.reset_instructions()

      self.handlers = defaultdict(lambda: {})

   def reset():
      Node.nodes.clear()
      del Node.loghistory[:]
      Node.lognum = 0
      Node().log("System reset")

   def clear_bufs(self):
      self.notequeue = { 'UP': [], 'RIGHT': [], 'DOWN': [], 'LEFT': [] }
      self.inqueue = { 'UP': None, 'RIGHT': None, 'DOWN': None, 'LEFT': None }
      self.outqueue = { 'side': None, 'value': None }

   def reset_instructions(self):
      self.instructions = {}
      def set_acc(val):
         self.ACC = min(max(val, -999), 999)
      def mov(src, dest):
         val = src['get']()
         if val == None:
            self.debug("MOV: blocked")
            return False
         self.debug("MOV")
         dest['set'](val)
      def swp():
         self.debug("SWP");
         swap = self.ACC
         self.ACC = self.BAK
         self.BAK = swap
      def sav():
         self.debug("SAV");
         self.BAK = self.ACC
      def neg():
         self.debug("NEG")
         set_acc(-self.ACC)
      def nop():
         self.debug("NOP");
      def add(src):
         val = src['get']()
         if val == None:
            self.debug("ADD: blocked")
            return False
         self.debug(f"ADD {val}")
         set_acc(self.ACC + val)
      def sub(src):
         val = src['get']()
         if val == None:
            self.debug("SUB: blocked")
            return False
         self.debug(f"SUB {val}")
         set_acc(self.ACC - val)
      def makejmp(name, pred):
         def jmp(label):
            labelname = label['get']()
            if pred(self.ACC):
               self.debug(f"{name} {labelname}")
               self.IP = self.labels[labelname]
               self.madeProgression = True # Did make progression without touching IP
               return False # don't touch the newly set IP
            else:
               self.debug(f"{name} {labelname}: predicate failed")
         return jmp
      def jro(delta):
         val = delta['get']()
         if val == None:
            self.debug("JRO: blocked")
            return False
         self.debug(f"JRO {val}")
         oldIp = self.IP
         self.IP += val
         if self.IP < 0: self.IP = 0
         if self.IP >= len(self.script): self.IP = len(self.script)-1
         if self.IP != oldIp:
            self.madeProgression = True # Did make progression without touching IP
         return False # don't touch the newly set IP
      def hcf():
         exec("()"*7**6) # begone, stack!
      # (C)onstant, (R)egister, (L)abel, Value (any)
      self.instructions["MOV"] = { "ops": ["V", "R"], "handler": mov }
      self.instructions["ADD"] = { "ops": ["V"], "handler": add }
      self.instructions["SUB"] = { "ops": ["V"], "handler": sub }
      self.instructions["NEG"] = { "ops": [], "handler": neg }
      self.instructions["NOP"] = { "ops": [], "handler": nop }
      self.instructions["SWP"] = { "ops": [], "handler": swp }
      self.instructions["SAV"] = { "ops": [], "handler": sav }
      self.instructions["JMP"] = { "ops": ["L"], "handler": makejmp("JMP", lambda acc: True) }
      self.instructions["JEZ"] = { "ops": ["L"], "handler": makejmp("JEZ", lambda acc: acc == 0) }
      self.instructions["JNZ"] = { "ops": ["L"], "handler": makejmp("JNZ", lambda acc: acc != 0) }
      self.instructions["JGZ"] = { "ops": ["L"], "handler": makejmp("JGZ", lambda acc: acc > 0) }
      self.instructions["JLZ"] = { "ops": ["L"], "handler": makejmp("JLZ", lambda acc: acc < 0) }
      self.instructions["JRO"] = { "ops": ["V"], "handler": jro }
      self.instructions["HCF"] = { "ops": [], "handler": hcf }

   def on(self, event, name, handler):
      self.handlers[event][name] = handler

   def emit(self, event, *args):
      cancelled = False
      for name in self.handlers[event]:
         if self.handlers[event][name](*args) == False:
            cancelled = True
      return cancelled

   def define(self, instr, ops, handler):
      self.instructions[instr.upper()] = { "ops": ops, "handler": handler }

   def register(self, rgstr, get, set):
      self.custom_registers[rgstr.upper()] = { "type": "R", "get": get, "set": set }

   # Pitch = 7 bits of the value
   # Velocity =
   # 3 bits of sequence # (6 sequences)
   # 2 bits of side # (up, right, down, left)
   # 1 bit of message type (VALUE_SEND, REQUEST_VALUE)
   # 1 bit data indication (and padding against 0vel)
   def request_value(self, side):
      type = 1
      sideid = ["DOWN", "LEFT", "UP", "RIGHT"].index(side)
      self.node.play_note(0, 1 + (type << 1) + (sideid << 2), 1/128)

   def gate(pitch, vel, mev):
      if vel & 0b1 == 0:
         mev.note_msg(pitch, vel)
         return True
      return False

   def proxy(target, pitch, vel, mev):
      if vel & 0b1 == 0 or vel > 0b1111111: return
      sideindex = (vel & 0b1100) >> 2
      side = ["UP", "RIGHT", "DOWN", "LEFT"][sideindex]
      if side == target.upper():
         mev.play_note(pitch, vel)
         return True
      return False

   def send_value(self, side, value):
      value = math.floor(value + (2**31-1))
      if value < 0: value = 0;
      if value > 2**32-1: value = 2**32-1;
      # reversed lookup since it's opposite on the other side
      sideid = ["DOWN", "LEFT", "UP", "RIGHT"].index(side)
      type = 0
      if self.emit("send_value", side, value) == False:
         self.node.play_note((value >> 7*0) & (2**7-1), 1 + (type << 1) + (sideid << 2) + (0 << 4), 1/64);
         self.node.play_note((value >> 7*1) & (2**7-1), 1 + (type << 1) + (sideid << 2) + (1 << 4), 1/64);
         self.node.play_note((value >> 7*2) & (2**7-1), 1 + (type << 1) + (sideid << 2) + (2 << 4), 1/64);
         self.node.play_note((value >> 7*3) & (2**7-1), 1 + (type << 1) + (sideid << 2) + (3 << 4), 1/64);
         self.node.play_note((value >> 7*4) & (2**7-1), 1 + (type << 1) + (sideid << 2) + (4 << 4), 1/64);
         self.node.play_note((value >> 7*5) & (2**7-1), 1 + (type << 1) + (sideid << 2) + (5 << 4), 1/64);

   def send_nonvalue_note(self, pitch, vel, length=1/16, pan=0, index=0):
      if vel == 1: vel = 2; # prevent 1 rounding to 0
      vel = vel & 0b1111110; # make sure last bit is always 0 to prevent interpretation as a data message
      if self.emit("send_nonvalue_note", pitch, vel, length, pan, index) == False:
         self.node.play_note(pitch, vel, length, pan, index);

   def recv_note(self, pitch, vel):
      if vel & 0b1 == 0 or vel > 0b1111111: return # Just a regular note, not a message (hopefully)

      type = (vel & 0b10) >> 1;
      series = (vel & 0b1110000) >> 4
      value = pitch << (series * 7)
      sideindex = (vel & 0b1100) >> 2
      side = ["UP", "RIGHT", "DOWN", "LEFT"][sideindex]

      if type == 1: # REQUEST_VALUE
         if self.emit("recieved_request", side) == False:
            if self.outqueue['side'] == side or self.outqueue['side'] == "ANY":
               retside = ["DOWN", "LEFT", "UP", "RIGHT"][sideindex]
               self.send_value(side, self.outqueue['value'])
               if self.outqueue['side'] == "ANY":
                  self.LAST = side
               self.outqueue['side'] = None
               self.outqueue['value'] = None;

      if type == 0: # VALUE_SEND
         queue = self.notequeue[side]
         queue.append(value)

         if len(queue) == 6:
            value = sum(queue) - (2**31-1)
            if self.emit("recieved_value", side, value) == False:
               self.inqueue[side] = value
            del queue[:]

   def parse_script(self, script, node):
      self.node = node;
      if self.lastScript == script: return
      self.lastScript = script;
      self.IP = 0;
      self.ACC = 0;
      self.BAK = 0;
      self.LAST = None;
      self.ANY_ITER = 0;
      self.labels = {}
      self.instrExecuted = 0;
      self.madeProgression = True
      self.clear_bufs();
      emptylines = 0

      def parse_arg(entry, lineno):
         try:
            val = int(entry)
            return { "type": "C", "get": lambda: val, "set": lambda x: 0 }
         except ValueError:
            pass

         if entry in self.custom_registers:
            return self.custom_registers[entry]

         if entry == "ACC":
            def set_acc(x): self.ACC = x
            return { "type": "R", "get": lambda: self.ACC, "set": set_acc }

         if entry == "NIL":
            return { "type": "R", "get": lambda: 0, "set": lambda x: 0 }

         if entry in ["UP", "RIGHT", "DOWN", "LEFT", "ANY", "LAST"]:
            def get():
               if entry == "LAST" and self.LAST is None: return 0;

               true_entry = entry
               if entry == "LAST": true_entry = self.LAST;
               if entry == "ANY":
                  self.ANY_ITER = (self.ANY_ITER + 1) % 4;
                  true_entry = ["LEFT", "RIGHT", "UP", "DOWN"][self.ANY_ITER];
                  self.debug(f"ANY_ITER {self.ANY_ITER} -> {true_entry}")

               val = self.inqueue[true_entry]
               if val is None:
                  self.request_value(true_entry)
                  return None
               self.inqueue[true_entry] = None
               self.LAST = true_entry
               self.ANY_ITER = 0
               return val

            def set(val):
               if entry == "LAST":
                  if self.LAST is None:
                     self.outqueue['side'] = "NO_LAST" # will block forever
                  else:
                     self.outqueue['side'] = self.LAST;
               else:
                  self.outqueue['side'] = entry
               self.outqueue['value'] = val

            return { "type": "R", "get": get, "set": set }

         return { "type": "L", "get": lambda: entry, "set": lambda x: 0 }

      def parse_instruction(instr, args, lineno):
         if instr in self.instructions:
            instruction = self.instructions[instr]

            if len(args) != len(instruction['ops']):
               ex = f"Incorrect number of arguments to {instr} on line {lineno}, "
               ex += f"expected {instruction['ops']} got {len(args)}"
               raise Exception(ex);

            for (arg, op) in zip(args, instruction['ops']):
               if op == "R" and arg['type'] != "R":
                  raise Exception(f"Expected register for {instr} on line {lineno}")
               if op == "C" and arg['type'] != "C":
                  raise Exception(f"Expected constant for {instr} on line {lineno}")
               if op == "L" and arg['type'] != "L":
                  raise Exception(f"Expected label for {instr} on line {lineno}")
               if op == "V" and arg['type'] not in ["R", "C"]:
                  raise Exception(f"Expected register or constant for {instr} on line {lineno}")
               # Otherwise, it's good

            handler = partial(instruction['handler'], *args)
            return { "handler": handler, "args": args, "line": lineno }

         raise Exception("Unknown instruction " + instr  + " on line " + str(lineno));

      def parse_line(line):
         nonlocal emptylines
         (lineno, entry) = line
         lineno -= emptylines

         labelpat = "^\\s*(\\w+):\\s*"
         label = re.match(labelpat, entry)
         if not label is None:
            name = label.group(1)
            self.labels[name] = lineno
            entry = re.sub(labelpat, "", entry)

         entry = re.sub(r"#.+$", "", entry).strip()
         if len(entry) == 0:
            emptylines += 1
            return None

         instr, *args = re.split("[ ,]+", entry)
         args = list(map(lambda arg: parse_arg(arg, lineno), args))
         instruction = parse_instruction(instr, args, lineno)

         return instruction

      cmds = script.upper().split('\n')
      cmds = map(parse_line, enumerate(cmds))
      cmds = filter(lambda a: a is not None, cmds)
      cmds = list(cmds)
      if len(cmds) == 0:
         self.madeProgression = False
      for instr in cmds:
         for arg in instr['args']:
            if arg['type'] == "L" and arg['get']() not in self.labels:
               raise Exception(f"Unknown label {arg['get']()} on line {instr['line']}")
      self.script = cmds

   def step(self):
      self.madeProgression = False
      if len(self.script) == 0: return
      if not self.outqueue['side'] is None: # write-blocked
         side = self.outqueue['side']
         value = self.outqueue['value']
         self.emit("step", None)
         self.debug(f"Write-blocked on side {side} ({value})")
         return
      if self.IP >= len(self.script): self.IP = 0

      instruction = self.script[self.IP]
      if self.emit("step", instruction) == False:
         if instruction['handler']() != False:
            self.madeProgression = True
            self.instrExecuted += 1;
            self.IP += 1

   def status(self):
      self.debug(f"IP {self.IP} ACC {self.ACC} BAK {self.BAK} ExecCount: {self.instrExecuted}")

   def debug(self, msg):
      if self.node is None: return;
      self.node.output(msg)

   def log(self, msg):
      Node.lognum += 1
      del Node.loghistory[:-20]
      Node.loghistory.append(f"[{Node.lognum:04}] {msg}")
      bespoke.set_background_text("\n".join(Node.loghistory), 16)


class ArgumentHelper:
   def __init__(self):
      self.reset()

   def reset(self):
      self.values = defaultdict(lambda: None)

   def provide(self, dict):
      for name in dict:
         if self.values[name] is not None: continue;
         self.values[name] = dict[name]['get']()

   def get(self, name):
      if not name in self.values: return None;
      return self.values[name];

   def ready(self):
      for key in self.values:
         if self.values[key] == None:
            return False;
      return True;

try: bespoke
except NameError:
   class NodeDebugger:
      def output(self, msg):
         print(msg)
      def play_note(self, *args):
         print("play_note", *args)
   # Terminal testing
   node = Node.get("test")
   node.parse_script("""
      # mov LEFT RIGHT
      jro 0
      loop:
         sub 1
      jmp loop
   """, NodeDebugger())
   for x in range(10):
      node.step();
      node.status();
