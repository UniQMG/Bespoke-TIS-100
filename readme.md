# Bespoke TIS-100
[TIS-100](https://www.zachtronics.com/tis-100/) interpreter for [Bespoke](https://bespokesynth.com/)

See [wiki](https://gitlab.com/UniQMG/Bespoke-TIS-100/-/wikis/home) for example setup, or read comments in each script file.


# The Node class
`Node.py` contains most of the low-mid level logic.

## Useful methods
- static `nodes.clear()`: Clears all active nodes, essentially resetting the system
- `define(name, ops, handler)`: Defines a custom instruction. Ops is a list of either `V`alues (register reads or literals), `R`egisters, `C`onstants or `L`abels defining the parameters passed to the handler. See example_node_2_with_play.py for examples.
- `register(name, get, set)`: Defines a custom register. `get` is 0 arguments and should return None only when blocking. `set` is 1 arguments and should never block. If you need to pause the script after a `set`, set `node.outqueue['side']` to some unique value and clear it later once
finished (this sets the script into write-block mode, and is primarily used for `MOV * LEFT/RIGHT/UP/DOWN` then cleared internally in `recv_note`).
- `on(name, name, handler)`: Registers an event listener. Name should be a unique string constant, which will overwrite the previously set handler of that name (to remain idempotent).
- `parse_script(script, node)`: Call from `on_pulse`. Sets the script of the node and resets the state if it changes, to remain idempotent.
- `step()`: Call from `on_pulse`. Runs the next instruction.
- `status()`: outputs node status (IP, ACC, BAK values)
- `recv_note(pitch, vel)`: Call from `on_note` for a node script
- `proxy(source, pitch, vel)`: Call from `on_note` in a note proxy to limit which way notes can flow. Returns `True` if the note was passed along.
- `gate(pitch, vel)`: Call from `on_note` in a note gate to allow only non-data notes. Returns `True` if the note was passed along.

## Events
- `send_value(side, value)`: Fired when `send_value` is called
- `send_nonvalue_note(pitch, vel, length, pan, index)`: Fired when `send_nonvalue_note` is called
- `step(instruction)`: Fired when an instruction is about to be executed via `step`. Instruction is None when write-blocked.
- `recieved_request(side)`: Fired when a read-request is recieved.
- `recieved_value(side, value)`: Fired when a value is finished being read.

return `False` from a handler to cancel the event
