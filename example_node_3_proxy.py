# An interconnect script for making sure values flow in the right direction
# HOW TO USE: see example_node_1.py

def on_note(pitch, vel):
   Node.proxy("left", pitch, vel, me.me())
