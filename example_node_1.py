# An empty TIS-100 node placeholder
# HOW TO USE:
# Connect to a example_node_3_proxy.py script, then connect that script
# to another example_node_1.py (with script tweaked to do something with LEFT input)
# Then, create another example_node_3_proxy.py srcipt and connect the other
# script back to this one. Repeat for all desired directions.
def on_note(pitch, vel):
   Node.get(0).recv_note(pitch, vel);
def on_pulse():
   node = Node.get(0);
   script  = ""
   script += "\n" # 1
   script += "\n" # 2
   script += "\n" # 3
   script += "\n" # 4
   script += "\n" # 5
   script += "\n" # 6
   script += "\n" # 7
   script += "\n" # 8
   script += "\n" # 9
   script += "\n" # 10
   script += "\n" # 11
   script += "\n" # 12
   script += "\n" # 13
   script += "\n" # 14
   script += "\n" # 15
   node.parse_script(script, me.me());
   node.step();
   node.status();
