# Visualization module node
# HOW TO USE:
# Write X, Y, one or more COLORs (0-4), then -1 terminator to it
# Extensions to base game:
# Set COLOR to -2 then read COLOR value at x,y back (0-4)
import grid

def recieved_value(side, value):
   WIDTH = 30
   HEIGHT = 18

   g = grid.get("grid") # make sure you spawned a grid with id "grid"!
   g.set_grid(WIDTH, HEIGHT)
   g.set_color(0, 1, 1, 1) # white but unlit
   g.set_color(1, 0.27, 0.27, 0.27)
   g.set_color(2, 0.61 * 255, 0.61, 0.61)
   g.set_color(3, 1, 1, 1)
   g.set_color(4, 0.75, 0.04, 0.04)

   node = Node.get(9)
   node.gridqueue.append(value)

   if value == -1:
      del node.gridqueue[:];

   elif value == -2:
      [x, y, *_] = node.gridqueue
      node.outqueue['side'] = "ANY"
      node.outqueue['value'] = g.get(x,y) and g.get_cell_color(x, y)
      del node.gridqueue[:];

   elif len(node.gridqueue) >= 3:
      [x, y, col] = node.gridqueue
      node.gridqueue = [x+1, y]
      g.set(x, y, col)
      g.set_cell_color(x, y, col)

   return False

def on_pulse():
   node = Node.get(9);
   if not hasattr(node, "gridqueue"):
      node.gridqueue = []

   node.parse_script("", me.me())
   node.on("recieved_value", "handler", recieved_value)

   if node.outqueue['side'] is None:
      node.request_value("UP");
      node.request_value("LEFT");
      node.request_value("RIGHT");
      node.request_value("DOWN");

def on_note(pitch, vel):
   Node.get(9).recv_note(pitch, vel);
